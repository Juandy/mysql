/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

DROP PROCEDURE IF EXISTS Facturacion;

DELIMITER //

CREATE PROCEDURE Facturacion()

BEGIN

    /*ALTER TABLE Comisiones DROP FOREIGN KEY FK_CodigoFacturas_Facturas;
    ALTER TABLE Facturas DROP PRIMARY KEY;*/
    DROP TABLE IF EXISTS ActualizacionLimiteCredito;
    DROP TABLE IF EXISTS Facturas;
    DROP TABLE IF EXISTS Comisiones;
    DROP TABLE IF EXISTS AsientoContable;

    CREATE TABLE ActualizacionLimiteCredito (
        CodigoCliente INT,
        Fecha DATE,
        Incremento NUMERIC(15,2),
        CONSTRAINT FK_Actualiza_Clientes FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
    );

    CREATE TABLE Facturas (
        CodigoCliente INT,
        BaseImponible NUMERIC(15,2),
        IVA NUMERIC(15,2),
        Total NUMERIC(15,2),
        CodigoFactura INT,
        PRIMARY KEY (CodigoFactura),
        CONSTRAINT FK_CodigoCliente_Clientes FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
    );


    CREATE TABLE Comisiones (
        CodigoEmpleado INT,
        Comision NUMERIC(15,2),
        CodigoFactura INT,
        CONSTRAINT FK_CodigoEmpleado_Empleados FOREIGN KEY (CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado),
        CONSTRAINT FK_CodigoFacturas_Facturas FOREIGN KEY (CodigoFactura) REFERENCES Facturas(CodigoFactura)
    );

    CREATE TABLE AsientoContable (
        CodigoCliente INT,
        DEBE NUMERIC(15,2),
        HABER NUMERIC(15,2),
        CONSTRAINT FK_CodigoCliente_Clientes_2 FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
    );

END//

DELIMITER ;

CALL Facturacion();

