/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

DROP PROCEDURE IF EXISTS Facturar;

DELIMITER //

CREATE PROCEDURE Facturar()
BEGIN

  DECLARE Facturado NUMERIC(15,2);
  DECLARE PrecioImponible   NUMERIC(15,2);
  DECLARE codeFactura INT;
  DECLARE comiEmpleado INT;
  DECLARE comiIncremento NUMERIC(15,2);
  DECLARE clientePedido  INT;
  DECLARE ultimoCliente INT;
  DECLARE ultimoClientePedido INT;

  /* Variables para calcular bucle */
  DECLARE x INT;
  DECLARE y INT;
  SET x = 1;
  SET y = 1;

  /* Contamos hasta el ultimo cliente, para empezar un bucle */
  SET ultimoCliente        =   (SELECT COUNT(*) FROM Pedidos);           /* Numero de Pedidos */
  SET ultimoClientePedido  =   (select MAX(CodigoPedido) FROM Pedidos);  /* Codigo Pedido mayor */

  /* El primer bucle incrementara CodigoCliente, hasta el ultimo contado */
  /* El segundo bucle incrementara CodigoPedido, hasta el ultimo que se ha contado */
  /* Si no se hace asi, te devolvera que se han introduzido muchas lineas a la vez */
  WHILE x <= ultimoClientePedido DO
    WHILE y <= ultimoClientePedido DO

      SET codeFactura =  (SELECT CodigoPedido FROM Pedidos WHERE CodigoCliente = x AND CodigoPedido = y);                               
      SET clientePedido  =  (SELECT CodigoCliente FROM Pedidos WHERE CodigoCliente = x AND CodigoPedido = y);                              
      SET Facturado = (SELECT SUM(Cantidad * PrecioUnidad) AS 'Precio total' FROM DetallePedidos WHERE CodigoPedido = y);  
      SET PrecioImponible  =  (SELECT SUM((Cantidad * PrecioUnidad) / 1.21) AS 'Total €' FROM DetallePedidos WHERE CodigoPedido = y);

        /* Si esta facturado y hay codigo pedido, insertara los datos */
        IF Facturado IS NOT NULL THEN
            IF clientePedido IS NOT NULL THEN

              /* Insertamos los datos en la tabla Facturas */
              INSERT INTO Facturas (CodigoFactura, CodigoCliente, IVA, BaseImponible, Total) VALUES (codeFactura, clientePedido, 0.15,PrecioImponible, Facturado);



              /* Aqui cogeremos los datos para la tabla de Comisiones */
              SET comiEmpleado = (SELECT CodigoEmpleadoRepVentas FROM Clientes WHERE CodigoCliente = clientePedido);
              SET comiIncremento = (SELECT SUM((Cantidad * PrecioUnidad)*0.05) AS 'Total €' FROM DetallePedidos WHERE CodigoPedido = y);

              /* Igual que antes, si no es nulo, se insertaran los datos */
              IF comiIncremento IS NOT NULL THEN
                INSERT INTO Comisiones(CodigoFactura, CodigoEmpleado, Comision) VALUES (codeFactura, comiEmpleado, comiIncremento);
        END IF;
        END IF;
        END IF;
      SET y = y + 1;
    END WHILE;
    /* Si ultimoCliente es menor al numero de pedidos, y valdra 1, porque si no, se duplicaba y se llenaba Facturas */
    IF y > ultimoCliente THEN
        SET y = 1;
    END IF;
    
    SET x = x + 1;
  END WHILE;
END//

DELIMITER ;

CALL Facturar();


