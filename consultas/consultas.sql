/*Mostrar que productos hay con su codigo nombre y tipo, cuantos productos hay*/
SELECT Productos.CodigoProducto,Productos.Nombre,Productos.Gama FROM Productos;
/*Mostrar solo los productos que se vendan por encima de 25€*/
SELECT Productos.CodigoProducto, Productos.Nombre, Productos.PrecioVenta FROM Productos WHERE Productos.PrecioVenta >= 25;
/*Mostrar el productos mas caro*/
SELECT Productos.CodigoProducto, Productos.Nombre, Productos.PrecioVenta FROM Productos ORDER BY PrecioVenta DESC LIMIT 2;
/*Sacar prodcutos que empiecen por M*/
SELECT  DISTINCT Productos.Nombre FROM Productos WHERE Productos.Nombre LIKE 'M%';
/*m minuscula*/
SELECT  DISTINCT Productos.Nombre FROM Productos WHERE Productos.NOmbre LIKE ('m%') COLLATE utf8_bin;
/*Pedidos entregados a tiempo*/
SELECT * FROM Pedidos WHERE Pedidos.FechaEsperada >= Pedidos.FechaEntrega;
/*Pedidos no entregados*/
SELECT * FROM Pedidos WHERE FechaEntrega is NULL;
/*Intervalo de busqueda*/
SELECT Clientes.CodigoCliente,Clientes.Ciudad FROM Clientes WHERE Clientes.Ciudad IN ('Fuenlabrada','Madrid','Barcelona');
/*GROUP BY*/
SELECT Productos.Gama, COUNT(*) AS 'Total'FROM Productos GROUP BY Productos.Gama;
/*Ejercicio*/
/*Sacar el codigo de oficina de la ciudad donde hay oficinas*/
SELECT Oficinas.CodigoOficina, Oficinas.Ciudad From Oficinas;
/*Sacar cuántos clientes tiene cada país*/
SELECT Clientes.Pais, COUNT(*)AS NumeroClientes FROM Clientes GROUP BY Clientes.Pais;
/*Sacar cuál fue el pago medio en 2009*/
SELECT AVG(Pagos.Cantidad) AS PagoMedio From Pagos WHERE YEAR(Pagos.FechaPago)='2009%';
/*Sacar el codigo del cliente que ha echo el pedido más caro */
SELECT  DetallePedidos.CodigoPedido, SUM(DetallePedidos.Cantidad * DetallePedidos.PrecioUnidad)AS Total FROM DetallePedidos GROUP BY DetallePedidos.CodigoPedido ORDER BY Total DESC LIMIT 1;
/*Sacar la ciudad y el tlf de las oficinas de EEUU*/
SELECT  Oficinas.Ciudad, Oficinas.Pais, Oficinas.Telefono FROM Oficinas WHERE Oficinas.Pais='EEUU';
/*Sacar el Nombre , Apellidos, Email de los empleados a cargo de alberto soria*/
SELECT Empleados.CodigoEmpleado, CONCAT(Empleados.Nombre,' ',Empleados.Apellido1)AS Nombre,Empleados.CodigoJefe FROM Empleados WHERE Empleados.CodigoJefe =(SELECT Empleados.CodigoEmpleado FROM Empleados WHERE Empleados.Nombre='Alberto' AND Empleados.Apellido1='Soria');
/*Sacar el Cargo, Nombre , Apellidos, Email del jefe de la empresa*/
SELECT Empleados.Puesto, Empleados.Nombre,Empleados.Apellido1, Empleados.Email FROM Empleados WHERE Empleados.CodigoJefe IS NULL;
/*Sacar el nombre apellidos y cargo de aquellos que no seas representantes de ventas*/
SELECT Empleados.Puesto, Empleados.Nombre,Empleados.Apellido1, Empleados.Email FROM Empleados WHERE Empleados.CodigoJefe IS NULL;
/*Sacar el número de clientes que tiene la empresa*/
SELECT  COUNT(*)AS ClientesTotal FROM  Clientes;
