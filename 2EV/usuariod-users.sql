/*CREATE USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'tu_contrasena';
GRANT ALL PRIVILEGES ON * . * TO 'nombre_usuario'@'localhost';*/

CREATE USER IF NOT EXISTS 'Admin'@'localhost' IDENTIFIED BY 'Admin';
GRANT ALL PRIVILEGES ON users.* TO 'Admin'@'localhost';

CREATE USER IF NOT EXISTS 'Gerente'@'localhost' IDENTIFIED BY 'Gerente';
GRANT DELETE,INSERT,SELECT,UPDATE ON users.* TO 'Gerente'@'localhost';

CREATE USER IF NOT EXISTS 'Usuario'@'localhost' IDENTIFIED BY 'Usuario';
GRANT SELECT ON  users.PEDIDOS TO 'Usuario'@'localhost';
CREATE VIEW Usuarios AS SELECT * FROM CLIENTES WHERE Nombre;
GRANT UPDATE (Direccion,Telefono) ON  users.CLIENTES TO 'Usuario'@'localhost';

CREATE USER IF NOT EXISTS 'Cajero'@'localhost' IDENTIFIED BY 'Cajero';
GRANT SELECT,UPDATE ON users.PRODUCTOS TO 'Cajero'@'localhost';

CREATE USER IF NOT EXISTS 'Comercial'@'localhost' IDENTIFIED BY 'Comercial';
GRANT SELECT,INSERT,UPDATE ON users.PRODUCTOS TO 'Comercial'@'localhost';
GRANT SELECT,INSERT,DELETE,UPDATE ON users.CLIENTES TO 'Comercial'@'localhost';


/*DROP USER IF EXISTS Admin;
DROP USER IF EXISTS Usuario;
DROP USER IF EXISTS Cajero;
DROP USER IF EXISTS Comercial;*/

/*RENAME USER 'Usuario'@'localhost' TO 'Usuario1'@'localhost'; (Renombramos el usuario clientes)*/
