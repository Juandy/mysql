/*Obtener nombre del cliente , con mayor limite de credito.*/
SELECT Clientes.NombreCliente, Clientes.LimiteCredito FROM Clientes WHERE Clientes.LimiteCredito=(SELECT MAX(Clientes.LimiteCredito)FROM Clientes);
/*Obtener nombre, apellido 1  y cargo de los empleados que no representan a ningún cliente.*/
SELECT Empleados.CodigoEmpleado, Empleados.Nombre, Empleados.Apellido1, Empleados.Puesto FROM Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.CodigoEmpleadoRepVentas IS NULL;
/*Sacar un listado con el nombre de cada cliente y el nombre y apellido de su representante de ventas.*/
SELECT Clientes.NombreCliente, Empleados.Nombre, Empleados.Apellido1 FROM Clientes, Empleados WHERE Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado;
/*Mostrar el nombre de los clientes que no hayan realizado pagos junto con el nombre de sus representantes de ventas.*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre FROM Clientes NATURAL LEFT JOIN Pagos JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente IS NULL;
/*Listar las ventas totales de los productos que hayan facturado más de 3000€.
Se mostrará el nombre unidades vendidas, total facturado y total factura con impuestos 21%*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre FROM Clientes NATURAL LEFT JOIN Pagos JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente IS NULL;
/*Listar la dirección de las oficinas que tengan clientes en fuenlabrada.*/
SELECT Oficinas.CodigoOficina, Oficinas.LineaDireccion1, Clientes.Ciudad, Clientes.CodigoCliente FROM Oficinas NATURAL LEFT JOIN Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas WHERE Clientes.Ciudad = 'Fuenlabrada';
/*********************************************************************************/
SELECT Oficinas.CodigoOficina, Oficinas.LineaDireccion1, Clientes.Ciudad, Clientes.CodigoCliente FROM Oficinas LEFT JOIN (Empleados, Clientes) ON (Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas AND Oficinas.CodigoOficina=Empleados.CodigoOficina) WHERE Clientes.Ciudad = 'Fuenlabrada';
/*********************************************************************************/
SELECT Clientes.NombreCliente, Clientes.Ciudad, Oficinas.Ciudad, Oficinas.LineaDireccion1 FROM Oficinas RIGHT JOIN  Clientes ON Oficinas.Region=Clientes.Region WHERE Clientes.Ciudad="Fuenlabrada";
/********************************************************************************/
SELECT DISTINCT Oficinas.LineaDireccion1 FROM Clientes JOIN Empleados NATURAL  JOIN Oficinas on Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.Ciudad='Fuenlabrada';
/*Obtener el producto más caro.*/
SELECT Productos.CodigoProducto, Productos.Nombre, Productos.PrecioVenta FROM Productos WHERE PrecioVenta=(SELECT MAX(PrecioVenta) FROM Productos);
/*Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido.*/
SELECT DetallePedidos.CodigoPedido, DetallePedidos.CodigoProducto, Productos.Nombre, DetallePedidos.Cantidad FROM DetallePedidos NATURAL JOIN Productos WHERE Cantidad=(SELECT MAX(Cantidad) FROM DetallePedidos);
/*Extra(Sacar el máximo de pedidos en total)*/
SELECT DetallePedidos.CodigoProducto,SUM(DetallePedidos.Cantidad) FROM DetallePedidos GROUP BY DetallePedidos.CodigoProducto ORDER BY SUM(DetallePedidos.Cantidad);
/*Obtener los clientes cuya línea de crédito sea mayor que los pagos que haya realizado.*/
SELECT Clientes.CodigoCliente, Clientes.LimiteCredito, SUM(Pagos.Cantidad) FROM Clientes Natural JOIN  Pagos GROUP BY Clientes.Codigocliente HAVING Clientes.LimiteCredito> SUM(Pagos.Cantidad);
/*Sacar el producto que más unidades  tiene en stock y el que menos unidades tiene en stock.*/
SELECT Productos.CodigoProducto,Productos.Nombre, Productos.CantidadEnStock FROM Productos WHERE Productos.CantidadEnStock=(SELECT MAX(Productos.CantidadEnStock) FROM Productos) OR Productos.CantidadEnStock=(SELECT MIN(Productos.CantidadEnStock) FROM Productos);
/*Sacar el nombre de los clientes y el de los representantes junto con la ciudad de la oficina a la que pertenece el representante.*/
SELECT Clientes.NombreCliente, Clientes.Ciudad, Empleados.Nombre FROM Oficinas JOIN (Empleados, Clientes) ON (Empleados.CodigoEmpleado=Clientes.CodigoEmpleadoRepVentas AND Oficinas.CodigoOficina=Empleados.CodigoOficina);
/*Sacar la misma información que la anterior pero, solo los clientes que no haya hecho pago.*/
SELECT Clientes.NombreCliente, Empleados.Nombre, Oficinas.Ciudad FROM Pagos NATURAL RIGHT JOIN Clientes JOIN Empleados NATURAL JOIN Oficinas ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente  IS NULL;
/*Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes.reflexiva*/
SELECT Curritos.CodigoEmpleado, Curritos.Nombre, Curritos.Apellido1, Jefes.CodigoEmpleado, Jefes.Nombre, Jefes.Apellido1 FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe=Jefes.CodigoEmpleado;
/*obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido.*/
SELECT Clientes.NombreCliente FROM Clientes WHERE Clientes.CodigoCliente IN(SELECT DISTINCT Pedidos.CodigoCliente FROM Pedidos Where FechaEntrega <= FechaEsperada);
/*Sacar un listado de clientes indicando el nombre del cliente y cuántos pedidos a realizado.*/
SELECT Clientes.NombreCliente,COUNT(*) FROM Clientes LEFT JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente GROUP BY Clientes.CodigoCliente;
/*Sacar un listado de los nombres de los clientes y el total pagado por cada uno de ellos.*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, SUM(Pagos.Cantidad) FROM Clientes NATURAL JOIN Pagos GROUP BY Pagos.CodigoCliente;
/*Sacar el nombre de los clientes que han hecho pedidos en 2008.*/
SELECT Clientes.NombreCliente,Pedidos.FechaPedido FROM Clientes NATURAL JOIN Pedidos WHERE Pedidos.FechaPedido LIKE '2008';
/*Listar el nombre del clientes y el nombre y apellido de sus representantes de aquellos clientes que no hayan realizado pagos.*/
SELECT Clientes.CodigoCliente, CONCAT(Empleados.Nombre, " ", Empleados.Apellido1) AS Nombre FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado  WHERE Clientes.CodigoCliente NOT IN (SELECT Clientes.CodigoCliente FROM  Clientes NATURAL JOIN Pagos) ORDER BY Clientes.CodigoCliente;
/*Nombre del empleado, código del empleado , código de la oficina, nombre de la oficina y ciudad de oficina*/
SELECT Empleados.nombre, Empleados.CodigoEmpleado, Oficinas.CodigoOficina, Oficinas.Ciudad FROM Empleados NATURAL JOIN Oficinas;
/*Código y nombre del cliente, código y nombre del empleado*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre, Empleados.CodigoEmpleado FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;
/*Código y nombre del cliente  que tengan o no Rep De ventas*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre, Empleados.CodigoEmpleado FROM Clientes LEFT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;
/*Código y nombre del cliente  que tengan o no Rep De ventas pero con todos los empleados*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre, Empleados.CodigoEmpleado FROM Clientes RIGHT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;
/*Mostrar todos los nombre del cliente, el del empleado que represente al cliente, el número de pedidos realizados y la cantidad pagada*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre, SUM(Pagos.Cantidad), COUNT(*) FROM Clientes LEFT JOIN (Empleados, Pagos, Pedidos) ON (Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado AND Clientes.CodigoCliente=Pagos.CodigoCliente AND Clientes.CodigoCliente=Pedidos.CodigoCliente) GROUP BY Clientes.CodigoCliente;

  