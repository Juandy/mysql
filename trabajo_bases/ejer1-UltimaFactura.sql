/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

DROP FUNCTION  IF EXISTS UltimaFactura;

DELIMITER //
/*Funcion que nos muestra la ultima factura, si no hay ninguna factura devuelve un 1*/
CREATE FUNCTION UltimaFactura()

RETURNS INT DETERMINISTIC

BEGIN

    DECLARE Select_int INT;
    SET Select_int = (SELECT COUNT(*) FROM Facturas);
    /*condición para el cumplimiento del ejercicio*/
    IF Select_int = 0 THEN SET Select_int = 1;
END IF;
RETURN Select_int;

END//

DELIMITER ;

SELECT UltimaFactura();


