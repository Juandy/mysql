Trabajo hecho por Juan Diego Fernandez Arroyave  y Álvaro Serrano Lozano

Base de Datos

El trabajo consiste en 7 archivos SQL.

Instrucciones de uso.
Se recomienda cargar de nuevo la base de datos jardineria para su correcto funcionamiento
Se pueden ir metiendo los fichero uno por uno o si lo deseas automáticamente solo tienes que cargar el archivo carga.sql.

Este archivo contiene un source a
    *crearTablas.sql
    *ejer0-almacenado.sql
    *ejer1-UltimaFactura.sql
    *ejer2-Facturar.sql
    *ejer3-GenerarContabilidad.sql

El archivo elegir.sql se deberá introducir a mano para una mejor corrección


