INSERT INTO ACTORES (Nombre, Fecha) VALUES 
('Ewan McGregor','1971-03-31'),
('Hayden Christensen','1981-04-19'),
('Ian McDiarmid','1944-08-11');

INSERT INTO NAVES (NºTripulantes, Nombre)  VALUES
('15','Nave 1'),
('20','Nave 2'),
('30','Nave 3');

INSERT INTO PELICULAS (Titulo, Director, Año) VALUES 
('Star Wars Episodio IV: Una nueva esperanza','Chris Columbus','1977'), 
('Star Wars Episodio V: El Imperio contraataca','Chris Columbus','1980'),
('Star Wars Episodio III: La venganza de los Sith','Alfonso Cuaron','2005');

INSERT INTO PLANETAS (Galaxia, Nombre)  VALUES
('Andromeda','Andro'),
('Via Lactea','Latea'),
('Sombrero','Somb');

INSERT INTO PERSONAJES (Nombre,GRADO,Codigo_ACTORES,CodigoSuperior_PERSONAJES) VALUES
('ObiWan-KenoVe',7,1,NULL),
('Lucas SkyWalker',9,2,1),
('Emperador Palpame',10,3,NULL);












