/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

DROP PROCEDURE IF EXISTS almacenado;

DELIMITER //

CREATE PROCEDURE almacenado()
BEGIN

    DECLARE   Fecha DATE;
    DECLARE   codeCliente INT;
    DECLARE   ultimoCliente INT;
    DECLARE   Porciento NUMERIC(15,2);
    DECLARE   AumentarLimiteC   NUMERIC(15,2);
    DECLARE   x INT;

    /* Para empezar el bucle, contamos los clientes que hay */
    SET ultimoCliente  = (SELECT COUNT(*) FROM Clientes ORDER BY CodigoCliente); 
    SET Fecha = (SELECT CURDATE()); /*Fecha actual (YYYY-MM-DD)*/
    SET x = 1;

    /* El bucle acabara cuando llegue al ulimo cliente contado en ultimoCliente */
    WHILE x <= ultimoCliente DO

    /* Guardamos los codigos cliente, la x es necesaria para ir de uno en uno */
    SET codeCliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = x);

    /* Si no hay codigo del cliente, no se realiza la actualizacion, como antes pusimos x, ira de un uno  */
    IF codeCliente IS NOT NULL then
      SET Porciento = (SELECT SUM(Cantidad)*0.15 AS '15%' FROM Pagos WHERE CodigoCliente = x AND YEAR(FechaPago) BETWEEN 2008 AND 2010);
    END IF;

    /*  Si el cliente no paga, no se calcula, entonces en la tabla seria null, como se trata de dinero lo ponemos como 0 */
    IF Porciento IS NULL THEN
      SET Porciento = 0;
    END IF; 

    IF codeCliente IS NOT NULL THEN
      INSERT INTO ActualizacionLimiteCredito VALUES (codeCliente, Fecha, Porciento);
    END IF;

    /* Aqui empezaremos a aumentar el limite de credito del cliente, uno a uno (x) */
    IF codeCliente IS NOT NULL THEN
      SET AumentarLimiteC = (SELECT Incremento FROM ActualizacionLimiteCredito WHERE CodigoCliente = x);
    END IF;



    /* Aqui finalmente si no es nulo, aumentara el porciento del LimiteCredito */
    IF codeCliente IS NOT NULL THEN
      UPDATE Clientes SET LimiteCredito = LimiteCredito + Porciento WHERE CodigoCliente = x;
    END IF;

    /*Aumentamos i para empezar de nuevo con el bucle*/
    SET x = x+1;

    END WHILE;

END//

DELIMITER ;

CALL almacenado();








