/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

DROP PROCEDURE IF EXISTS GenerarContabilidad;

DELIMITER //

/*Procedimiento que genera el dinero  gastado por cada cliente en todos los pedidos*/
CREATE PROCEDURE GenerarContabilidad()
BEGIN

 /* Declaro variables*/
  DECLARE Gastos NUMERIC(15,2);
  DECLARE clientesNum INT;
  DECLARE Cliente INT;
  DECLARE x INT;

  SET x = 0;

  /* Metemos el numero de clientes en clientesNum */
  SET clientesNum = (SELECT COUNT(*) FROM Clientes);
  /* Este bucle pondra los clientes uno a uno */
  WHILE x <= clientesNum DO
    SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = x);
    IF Cliente IS NOT NULL THEN /* Si no es nulo, metera en gastos la suma total de Facturas*/
      SET Gastos = (SELECT SUM(Total) AS 'Total €' FROM Facturas WHERE CodigoCliente = x GROUP BY CodigoCliente);

/* Si Gastos es nulo, lo pondra como 0 */
/* Como se tratata de dinero, lo ponemos como 0 y para que no se vea como NULL) */
      IF Gastos IS NULL THEN
        SET Gastos = 0; 
      END IF;
      
      /* El bucle  tambien ira metiendo los valores de Cliente y Gastos en el codigo del cliente y lo que DEBE */
      INSERT INTO AsientoContable (CodigoCliente, DEBE, HABER) VALUES (Cliente, Gastos, Gastos);
    END IF;

    SET x = x + 1; /* Para que se repita el bucle */
  END WHILE;

END//

DELIMITER ;

CALL GenerarContabilidad();


