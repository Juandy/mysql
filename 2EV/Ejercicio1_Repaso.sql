DROP DATABASE IF EXISTS Ejercicio1_Repaso;
CREATE DATABASE Ejercicio1_Repaso;

USE Ejercicio1_Repaso;

CREATE TABLE PERSONA(nombre VARCHAR(20), apellidos VARCHAR(20), trabajo VARCHAR (30), PRIMARY KEY(nombre, apellidos));
CREATE TABLE SITUACION(hora TINYINT UNSIGNED, lugar VARCHAR(30), nombre_PERSONA VARCHAR(20), apellidos_PERSONA VARCHAR(20), vestuario VARCHAR(20), mercancia VARCHAR(20), PRIMARY KEY(hora), FOREIGN KEY(nombre_PERSONA, apellidos_PERSONA) REFERENCES PERSONA(nombre, apellidos));
CREATE TABLE OBJETO(nombre VARCHAR(20), tamaño ENUM('pequeño','mediano','grande'), nombre_OBJETO_contenedor VARCHAR(20), PRIMARY KEY(nombre), FOREIGN KEY(nombre_OBJETO_contenedor) REFERENCES OBJETO(nombre));
CREATE TABLE lleva(hora_SITUACION TINYINT UNSIGNED, nombre_OBJETO VARCHAR(20), FOREIGN KEY(hora_SITUACION) REFERENCES SITUACION(hora), FOREIGN KEY(nombre_OBJETO) REFERENCES OBJETO(nombre));





/*Datos*/

INSERT INTO PERSONA VALUES
('Juan', 'Sánchez', 'Informatico'),
('Laura', 'Fernandez', 'Informatica'),
('Jose Maria', 'De la Osa', 'Encargado de mantenimiento'),
('Pepe', 'Reina', 'Enfermero');


INSERT INTO OBJETO(nombre, tamaño) VALUES('Mochila', 'mediano');
INSERT INTO OBJETO(nombre, tamaño) VALUES('Carpeta', 'mediano');
INSERT INTO OBJETO VALUES('Martillo', 'grande', 'Mochila');
INSERT INTO OBJETO VALUES('USB', 'pequeño', 'Mochila');
INSERT INTO OBJETO VALUES('Folios', 'mediano', 'Carpeta');
INSERT INTO SITUACION VALUES(16, 'Parque', 'Juan', 'Sánchez', 'Informal', 'Bolsa');
INSERT INTO SITUACION VALUES(18, 'Casa de Juan', 'Jose Maria', 'De la Osa', 'Traje negro', 'Baby');
INSERT INTO SITUACION VALUES(20, 'Oficina', 'Pepe', 'Reina', 'uniforme blanco', 'Carrito compra');
INSERT INTO lleva VALUES(16, 'Martillo');
INSERT INTO lleva VALUES(18, 'Carpeta');
INSERT INTO lleva VALUES(20, 'USB');

/*Show tables*/
SELECT * FROM SITUACION;
SELECT * FROM OBJETO;
SELECT * FROM PERSONA;
SELECT * FROM lleva;



/*Datos curiosos*/
/*UPDATE PERSONA SET nombre='Maria' WHERE nombre='Laura';Cambio el nombre de Laura por Maria*/
/*INSERT INTO OBJETO VALUES('Mochila', 'mediano',NULL);*/

