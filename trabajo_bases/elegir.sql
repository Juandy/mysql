/*Trabajo Base de Datos*/
/*Hecho por Juandiego Fernandez Arroyave y Alvaro Serrano Lozano*/
/*Fecha de entrega 24 de mayo del 2019*/

/* Cantidad de productos que hay en un pedido */

DROP PROCEDURE IF EXISTS CantidadProductos;

DELIMITER //

CREATE PROCEDURE CantidadProductos()

BEGIN

    SELECT DISTINCT Productos.Nombre, DetallePedidos.Cantidad, DetallePedidos.CodigoPedido FROM DetallePedidos NATURAL JOIN Productos ORDER BY DetallePedidos.Cantidad;

END//

DELIMITER ;

CALL CantidadProductos();

