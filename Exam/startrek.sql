DROP DATABASE IF EXISTS startrek;
CREATE DATABASE startrek;

USE startrek;


CREATE TABLE Actores(
    Nombre VARCHAR(20),
    Personaje VARCHAR(30),
    FechaNacimiento DATE,
    Nacinalidad VARCHAR(20),
    CONSTRAINT PK_Actores PRIMARY KEY (Nombre)
    );


CREATE TABLE Personajes(
    Nombre VARCHAR(30),
    Raza VARCHAR(20),
    GraduacionMilitar ENUM ('capitan', 'Teniente', 'Almirante', 'Oficial'),
    NombreActor VARCHAR (20),
    NombrePersonajes VARCHAR (30),
    CONSTRAINT PK_Personajes PRIMARY KEY (Nombre),
    CONSTRAINT FK_PersonajesActores FOREIGN KEY (NombreActor) REFERENCES Actores (Nombre),
    CONSTRAINT FK_PersonajesPersonaje FOREIGN KEY (NombrePersonajes) REFERENCES Personajes (Nombre));

CREATE TABLE Naves(
    Nombre VARCHAR (30),
    Codigo INT UNSIGNED,
    NumTripulantes INT UNSIGNED,
    CONSTRAINT PK_Nave PRIMARY KEY (Codigo)
    );

CREATE TABLE Planetas (
    Codigo INT UNSIGNED,
    Nombre VARCHAR (30),
    Galaxia VARCHAR (40),
    Problema VARCHAR (255),
    CodigoNave INT UNSIGNED,
    CONSTRAINT PK_Planetas PRIMARY KEY (Codigo),
    CONSTRAINT FK_PlanetasNaves FOREIGN KEY (CodigoNave) REFERENCES Naves (Codigo)
    );
CREATE TABLE Capitulos(
    Numero INT UNSIGNED,
    Temporada INT UNSIGNED,
    Titulo VARCHAR (50),
    Orden INT UNSIGNED,
    CodigoPlanetas INT UNSIGNED,
    CONSTRAINT PK_Capitulos PRIMARY KEY (Numero,Temporada),
    CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlanetas) REFERENCES Planetas (Codigo)
    );

CREATE TABLE AparicionesSerie (
    NombrePersonajes VARCHAR (30),
    NumeroCapitulo INT UNSIGNED,
    NumeroTemporada INT UNSIGNED,
    CONSTRAINT FK_AparicionesSeries FOREIGN KEY (NombrePersonajes) REFERENCES Personajes (Nombre),
    CONSTRAINT FK_AparicionesCapitulo FOREIGN KEY (NumeroCapitulo, NumeroTemporada) REFERENCES Capitulos (Numero, Temporada)
    );

CREATE TABLE Peliculas (
    Titulo VARCHAR(30),
    AñoLanzamiento YEAR,
    Director VARCHAR (50),
    CONSTRAINT PK_Peliculas PRIMARY KEY (Titulo)
    );

CREATE TABLE AparicionesPelis(
    NombrePersonaje VARCHAR(30),
    TituloPelicula VARCHAR (30),
    Protagonista ENUM ('SI', 'NO'),
    CONSTRAINT FK_AparicionesPelis FOREIGN KEY (NombrePersonaje) REFERENCES Personajes (Nombre),
    CONSTRAINT FK_AparicionesPelisPelis FOREIGN KEY (TituloPelicula) REFERENCES Peliculas (Titulo)
    );
