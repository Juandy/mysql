DROP DATABASE IF EXISTS Ejercicio_Clase;
CREATE DATABASE Ejercicio_Clase;

USE Ejercicio_Clase;

CREATE TABLE ciudad (codigo INT, nombre VARCHAR (30), PRIMARY KEY (codigo));
CREATE TABLE temperatura (dia DATE,codigo_ciudad INT,grados INT, PRIMARY KEY (dia),FOREIGN KEY (codigo_ciudad) REFERENCES ciudad (codigo) );
CREATE TABLE humedad (dia DATE,codigo_ciudad INT, porcentaje VARCHAR(10),PRIMARY KEY (dia), FOREIGN KEY (codigo_ciudad) REFERENCES ciudad (codigo));
CREATE TABLE informe (ID INT AUTO_INCREMENT,codigo_ciudad INT,PRIMARY KEY (ID), FOREIGN KEY (codigo_ciudad) REFERENCES ciudad (codigo));







