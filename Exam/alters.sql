ALTER TABLE Capitulos DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Planetas MODIFY Codigo INT UNSIGNED AUTO_INCREMENT;
ALTER TABLE Capitulos ADD CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlanetas) REFERENCES Planetas(Codigo);

ALTER TABLE Capitulos DROP FOREIGN KEY FK_CapitulosPlanetas;
ALTER TABLE Capitulos ADD CONSTRAINT FK_CapitulosPlanetas FOREIGN KEY (CodigoPlanetas) REFERENCES Planetas (Codigo) ON UPDATE CASCADE;

ALTER TABLE Peliculas ADD Recaudacion DECIMAL (5,2);


INSERT INTO Actores VALUES ('Luke Perry','Spiderman','1990-02-02','Español');
INSERT INTO Actores VALUES ('Galindo','Ant-Man','1930-02-02','Marciana');
INSERT INTO Personajes VALUES ('Spiderman','Mutante','Oficial','Luke Perry',NULL);
INSERT INTO Naves VALUES ('Halcon Milenario',1,10);
INSERT INTO Planetas (Nombre,Galaxia,Problema,CodigoNave) VALUES('Kamino','Via Lactea','Nos enfrentamos mi capitan',1);
INSERT INTO Capitulos VALUES (1,1,'The Beginning Is the End Is the Beginning',3,1);
INSERT INTO Capitulos VALUES (1,2,'2x01',1,1);
INSERT INTO AparicionesSerie VALUES ('Spiderman',1,1);
INSERT INTO Peliculas VALUES ('Shin-Chan y las bolas mágicas',2007,'Nikito NiPongo LaPelota',999.50);
INSERT INTO AparicionesPelis VALUES ('Spiderman','Shin-Chan y las bolas mágicas','SI')


UPDATE AparicionesSerie SET NumeroTemporada='2' WHERE NumeroCapitulo='1' AND NumeroTemporada='1';
DELETE FROM Actores WHERE Nombre='Galindo'; /*Este si nos deja porque no depende nadie de el, a Luke Perry no se podria borrar ya que de Luke dependes otras tablas*/
